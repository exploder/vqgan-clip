from setuptools import setup


setup(
    name='vqclip-generator',
    packages=[
        'vqgan_clip',
    ],
    install_requires=[
        'kornia==0.6.2',
        'pillow==8.4.0',
        'torch-optimizer==0.3.0',
        'omegaconf==2.1.1',
        'torch @ https://download.pytorch.org/whl/cu102/torch-1.9.0%2Bcu102-cp39-cp39-linux_x86_64.whl',
        'torchvision @ https://download.pytorch.org/whl/cu102/torchvision-0.10.0%2Bcu102-cp39-cp39-linux_x86_64.whl',
        'taming-transformers @ git+https://gitlab.com/exploder/taming-transformers.git@540e45e#egg=taming-transformers',
        'clip @ git+https://github.com/openai/CLIP@573315e#egg=clip'
    ]
)

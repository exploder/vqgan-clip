import torch
from omegaconf import OmegaConf
from taming.models import cond_transformer, vqgan
from clip import clip

from .vqgan_clip import VQGANCLIP


class Generator:
    def __init__(self, vqgan_config_path, vqgan_model_path, clip_model_path):
        self._model_path = vqgan_model_path
        self._config_path = vqgan_config_path
        self._clip_path = clip_model_path
        self._model = None
        self._device = torch.device('cuda:0')
        self._perceptor = None

    def generate_image(self, text_prompt: str):
        generator = VQGANCLIP(
            self._device,
            self._model,
            self._perceptor,
            'Adam',
            (384, 256),
            prompts=[text_prompt]
        )
        generator.run()
        generator.image().save('test.png')

    def load(self):
        self._load_vqgan_model(self._config_path, self._model_path)
        self._model = self._model.to(self._device)
        self._perceptor = clip.load(self._clip_path, jit=False)[0]\
            .eval()\
            .requires_grad_(False)\
            .to(self._device)

    def unload(self):
        del self._model
        del self._perceptor
        self._model = None
        self._perceptor = None

    def _load_vqgan_model(self, config_path, checkpoint_path):
        gumbel = False
        config = OmegaConf.load(config_path)
        if config.model.target == 'taming.models.vqgan.VQModel':
            model = vqgan.VQModel(**config.model.params)
            model.eval().requires_grad_(False)
            model.init_from_ckpt(checkpoint_path)
        elif config.model.target == 'taming.models.vqgan.GumbelVQ':
            model = vqgan.GumbelVQ(**config.model.params)
            model.eval().requires_grad_(False)
            model.init_from_ckpt(checkpoint_path)
            gumbel = True
        elif config.model.target == 'taming.models.cond_transformer.Net2NetTransformer':
            parent_model = cond_transformer.Net2NetTransformer(
                **config.model.params)
            parent_model.eval().requires_grad_(False)
            parent_model.init_from_ckpt(checkpoint_path)
            model = parent_model.first_stage_model
        else:
            raise ValueError(f'unknown model type: {config.model.target}')
        del model.loss
        self._model = model

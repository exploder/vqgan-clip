import torch
import kornia.augmentation as K
from torch import nn, optim


class MakeCutouts(nn.Module):
    def __init__(self, cut_size, cutn, cut_pow=1., augment_types=None):
        super().__init__()
        if augment_types is None:
            augment_types = []
        self.cut_size = cut_size
        self.cutn = cutn
        self.cut_pow = cut_pow  # not used with pooling

        # Pick your own augments & their order
        augment_list = []
        for item in augment_types:
            if item == 'Ji':
                augment_list.append(
                    K.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1,
                                  hue=0.1, p=0.7))
            elif item == 'Sh':
                augment_list.append(K.RandomSharpness(sharpness=0.3, p=0.5))
            elif item == 'Gn':
                augment_list.append(
                    K.RandomGaussianNoise(mean=0.0, std=1., p=0.5))
            elif item == 'Pe':
                augment_list.append(
                    K.RandomPerspective(distortion_scale=0.7, p=0.7))
            elif item == 'Ro':
                augment_list.append(K.RandomRotation(degrees=15, p=0.7))
            elif item == 'Af':
                augment_list.append(
                    K.RandomAffine(degrees=15, translate=0.1, shear=5, p=0.7,
                                   padding_mode='zeros',
                                   keepdim=True))  # border, reflection, zeros
            elif item == 'Et':
                augment_list.append(K.RandomElasticTransform(p=0.7))
            elif item == 'Ts':
                augment_list.append(
                    K.RandomThinPlateSpline(scale=0.8, same_on_batch=True,
                                            p=0.7))
            elif item == 'Cr':
                augment_list.append(
                    K.RandomCrop(size=(self.cut_size, self.cut_size),
                                 pad_if_needed=True, padding_mode='reflect',
                                 p=0.5))
            elif item == 'Er':
                augment_list.append(
                    K.RandomErasing(scale=(.1, .4), ratio=(.3, 1 / .3),
                                    same_on_batch=True, p=0.7))
            elif item == 'Re':
                augment_list.append(
                    K.RandomResizedCrop(size=(self.cut_size, self.cut_size),
                                        scale=(0.1, 1), ratio=(0.75, 1.333),
                                        cropping_mode='resample', p=0.5))

        self.augs = nn.Sequential(*augment_list)
        self.noise_fac = 0.1
        # self.noise_fac = False

        # Uncomment if you like seeing the list ;)
        # print(augment_list)

        # Pooling
        self.av_pool = nn.AdaptiveAvgPool2d((self.cut_size, self.cut_size))
        self.max_pool = nn.AdaptiveMaxPool2d((self.cut_size, self.cut_size))

    def forward(self, input_data):
        cutouts = []

        for _ in range(self.cutn):
            # Use Pooling
            cutout = (self.av_pool(input_data) + self.max_pool(input_data)) / 2
            cutouts.append(cutout)

        batch = self.augs(torch.cat(cutouts, dim=0))

        if self.noise_fac:
            facs = batch.new_empty([self.cutn, 1, 1, 1]).uniform_(0,
                                                                  self.noise_fac)
            batch = batch + facs * torch.randn_like(batch)
        return batch

import torch
from clip import clip
from torchvision import transforms
from torch.nn import functional as F
from torchvision.transforms import functional as TF

from .cutouts import MakeCutouts
from .prompt import split_prompt, Prompt
from .util import vector_quantize, clamp_with_grad, get_opt


class VQGANCLIP:
    def __init__(
            self,
            device,
            model,
            perceptor,
            optimizer_name,
            output_dims,
            cut_count=4,
            prompts=None
    ):
        if prompts is None:
            prompts = []
        self._device = device
        self._model = model
        self._perceptor = perceptor
        self._normalize = transforms.Normalize(
            mean=[0.48145466, 0.4578275, 0.40821073],
            std=[0.26862954, 0.26130258, 0.27577711]
        )
        cut_size = perceptor.visual.input_resolution
        # TODO find out what this (f) is
        self._f = 2 ** (model.decoder.num_resolutions - 1)
        self._make_cutouts = MakeCutouts(
            cut_size,
            cut_count,
            cut_pow=1.,
            augment_types=['Af', 'Pe', 'Ji', 'Er']
        )
        self._iteration = 0
        self._tokens_x, self._tokens_y = \
            output_dims[0] // self._f, output_dims[1] // self._f
        self._side_x, self._side_y = \
            self._tokens_x * self._f, self._tokens_y * self._f
        # TODO find out what this (e_dim) is
        self._e_dim = model.quantize.e_dim
        self._token_count = model.quantize.n_e
        self._z_min = model.quantize.embedding.weight\
            .min(dim=0)\
            .values[None, :, None, None]
        self._z_max = model.quantize.embedding.weight\
            .max(dim=0)\
            .values[None, :, None, None]
        one_hot = F.one_hot(
            torch.randint(
                self._token_count,
                [self._tokens_y * self._tokens_x],
                device=device
            ),
            self._token_count
        ).float()

        # TODO find out what this (z) is
        self._z = one_hot @ model.quantize.embedding.weight
        self._z = self._z.view([
            -1,
            self._tokens_y,
            self._tokens_x,
            self._e_dim
        ]).permute(0, 3, 1, 2)
        self._z_orig = self._z.clone()
        self._z.requires_grad_(True)

        self._optimizer = get_opt(optimizer_name, 0.1, self._z)

        self._prompts = []
        for prompt in prompts:
            txt, weight, stop = split_prompt(prompt)
            embed = self._perceptor.encode_text(clip.tokenize(txt).to(device)).float()
            self._prompts.append(Prompt(embed, weight, stop).to(device))

    def run(self):
        while True:
            print(self._iteration)
            # Training time
            self._train()

            # Ready to stop yet?
            if self._iteration == 250:  # TODO better number of iters
                break
            self._iteration += 1

    def image(self):
        out = self._synth()
        return TF.to_pil_image(out[0].cpu(), mode='RGB')

    def _train(self):
        self._optimizer.zero_grad(set_to_none=True)
        loss = sum(self._ascend_txt())
        print(loss)
        loss.backward()
        self._optimizer.step()
        with torch.no_grad():
            self._z.copy_(self._z.maximum(self._z_min).minimum(self._z_max))

    def _ascend_txt(self):
        clip_encode = self._perceptor.encode_image(
            self._normalize(
                self._make_cutouts(self._synth())
            )
        ).float()

        result = []
        for prompt in self._prompts:
            result.append(prompt(clip_encode))
        return result

    def _synth(self):
        z_quantized = vector_quantize(
            self._z.movedim(1, 3),
            self._model.quantize.embedding.weight
        ).movedim(3, 1)
        return clamp_with_grad(
            self._model.decode(z_quantized).add(1).div(2),
            0,
            1
        )
